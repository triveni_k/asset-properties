import {OidAndValue} from "../db/models/oidAndValue";
let {promisify} = require('util');
let snmp = require("net-snmp");
import {Asset} from "../db/models/Asset";
import {ipAndOids} from "../db/models/ipsAndOids";
let session: any;

export class Snmp {
  getSnmpConnection(ip: string) {
    session = snmp.createSession(ip, "public");
    return session;
  }


  getAssetProperties(session: any, ipAndOid: ipAndOids) {
    return new Promise((resolve, reject) => {
      session.get(ipAndOid, function (error, varbinds) {
        if (error) {
          console.log("11111111", error);
        } else {
          for (var i = 0; i < varbinds.length; i++)
            if (snmp.isVarbindError(varbinds[i])) {
              console.log("22222222");
            } else {
              console.log("3333333", varbinds);
              resolve(varbinds)
            }
        }
      });
  });
  }
  closeSnmpConnection() {
    snmp.closeConnection();
  }
}