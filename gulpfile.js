var gulp = require("gulp"),
ts = require("gulp-typescript"),
tsProject = ts.createProject("tsconfig.json"),
browserify = require("browserify"),
source = require('vinyl-source-stream'),
sourcemaps = require('gulp-sourcemaps'),
merge = require('merge2'),
tsify = require("tsify"),
babelify = require("babelify"),
uglify = require("gulp-uglifyes"),
buffer = require("vinyl-buffer"),
paths = {
    pages: ['src/views/*.html']
},
compile = [
    'app',
];

gulp.task("copy-html", function () {
    return gulp.src(paths.pages)
        .pipe(gulp.dest("dist"));
});
gulp.task("bundle-Idb", function () {
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['./storage/indexedDbWrapper/IndexedDbRepository.ts'],
        cache: {},
        packageCache: {}
    }).plugin(tsify)
        .bundle()
        .pipe(source('indexedDb.cartos.js'))
        .pipe(buffer())
        .pipe(uglify({
            mangle: true,
            ecma: 6
        }))
        .pipe(gulp.dest("dist"));
});
gulp.task("bundle", ["ts", "copy-html"], function () {
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['./src/main/browser.js'],
        cache: {},
        packageCache: {},
        standalone: "CartosApp"
    })
        .transform(babelify.configure({
            presets: ['es2015'],
            extensions: ['.js', '.ts']
        }))
        .bundle()
        .pipe(source('core.cartos.js'))
        // .pipe(buffer())
        // .pipe(uglify({
        //   mangle: true,
        //   ecma: 6
        // }))
        .pipe(gulp.dest("dist/app"));
});

gulp.task("mps", ["ts", "copy-html"], function () {
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['./src/mps/mps.js'],
        cache: {},
        packageCache: {},
        standalone: "MPS"
    })
        .transform(babelify.configure({
            presets: ['es2015'],
            extensions: ['.js', '.ts']
        }))
        .bundle()
        .pipe(source('cartos.mps.js'))
        .pipe(buffer())
        .pipe(uglify({
            mangle: true,
            ecma: 5
        }))
        .pipe(gulp.dest("dist/app"));
});

gulp.task("ts", function () {
    let tsResult = gulp.src(compile, {base: '.'})
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .once("error", function () {
            this.once("finish", function () {
                process.exit(1);
            });
        });

    return merge([
        tsResult.dts.pipe(gulp.dest('.')),
        tsResult.js.pipe(sourcemaps.write('.')).pipe(gulp.dest('.')),
        tsResult.pipe(sourcemaps.write('.'))
    ]);
});
