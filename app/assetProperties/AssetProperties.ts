import {AssetPropertiesHelper} from "./getAssetPropertiesHelper";

let async = require("async");
const child= require('child_process');
let assetPropertyHelper = new AssetPropertiesHelper();

export class AssetProperty {
  getIpAddressAndOids() {
    return {
      ips: ["192.168.5.221", "192.168.4.57"],
      oids: []
    }
  }

  getAssetProperty(next: Function) {
    async.waterfall([
      (cb: Function) => {
        let ipsAndOids = this.getIpAddressAndOids();
        cb(null, ipsAndOids);
      },
      (ipsAndOids: any, cb: Function) => {
        for (let index = 0; index < ipsAndOids.ips.length; index++) {
          child.fork("./getAssetPropertiesHelper");
          let assetProperties = assetPropertyHelper.getAssetProperties(ipsAndOids.ips[index], ipsAndOids.oids, cb);
        }
      }], (err: any, res: any) => {
      if (err) {
        next(err, null);
      } else {
        next(null, res);
      }
    });
  }
}
let assetProperty = new AssetProperty();
assetProperty.getAssetProperty((err: any, data: any) => {
  if(err){

  }
});