let config = require("../config/config.js");
let snmp = require("net-snmp");
let async = require("async");


export class AssetPropertiesHelper {
  createSession(ip: string) {
    return snmp.createSession(ip, "public");
  }

  getAssetProperties(ip: string, oids: Array<string>, next: Function) {
    async.waterFall([
      (cb: Function) => {
        let session = this.createSession(ip);
        cb(session);
      },
      (session: any, cb: Function) => {
        this.getPropertiesByOid(session, oids, cb);
      },
      (err: any, assetProperties: any, cb: Function) => {
        session.close();
        cb(err, assetProperties);
      }], (err: any, AssetProperties: any) => {
      next(err, AssetProperties)
    });
  }

  getPropertiesByOid(session: any, oids: Array<string>, next: Function) {
    session.get(oids, function (error, varbinds) {
      if (error) {
        console.log("err", error)
        next(error, null);
      } else {
        for (var i = 0; i < varbinds.length; i++)
          if (snmp.isVarbindError(varbinds[i])) {
            console.error(snmp.varbindError(varbinds[i]));
            next(varbinds, null);
          } else {
            console.log("#######", varbinds[i].oid + " = " + varbinds[i].value);
            next(null, varbinds);
          }
      }
    });
  }
}

let asset = new AssetPropertiesHelper();
asset.getAssetProperties("192.168.4.57", ["1.3.6.1.2.1.5.23.0"], (err: any, data: any) => {
  console.log("#####", data, err);
});