export class Asset {
  assetProperties: any;
  ip: string;
  macAddress: string;
  sku: string;
  oids: object;

  constructor(ip: string){
    this.ip = ip;
  }
}